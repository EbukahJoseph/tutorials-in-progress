                      :-) GROMACS - gmx mdrun, 2020.3 (-:

                            GROMACS is written by:
     Emile Apol      Rossen Apostolov      Paul Bauer     Herman J.C. Berendsen
    Par Bjelkmar      Christian Blau   Viacheslav Bolnykh     Kevin Boyd    
 Aldert van Buuren   Rudi van Drunen     Anton Feenstra       Alan Gray     
  Gerrit Groenhof     Anca Hamuraru    Vincent Hindriksen  M. Eric Irrgang  
  Aleksei Iupinov   Christoph Junghans     Joe Jordan     Dimitrios Karkoulis
    Peter Kasson        Jiri Kraus      Carsten Kutzner      Per Larsson    
  Justin A. Lemkul    Viveca Lindahl    Magnus Lundborg     Erik Marklund   
    Pascal Merz     Pieter Meulenhoff    Teemu Murtola       Szilard Pall   
    Sander Pronk      Roland Schulz      Michael Shirts    Alexey Shvetsov  
   Alfons Sijbers     Peter Tieleman      Jon Vincent      Teemu Virolainen 
 Christian Wennberg    Maarten Wolf      Artem Zhmurov   
                           and the project leaders:
        Mark Abraham, Berk Hess, Erik Lindahl, and David van der Spoel

Copyright (c) 1991-2000, University of Groningen, The Netherlands.
Copyright (c) 2001-2019, The GROMACS development team at
Uppsala University, Stockholm University and
the Royal Institute of Technology, Sweden.
check out http://www.gromacs.org for more information.

GROMACS is free software; you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License
as published by the Free Software Foundation; either version 2.1
of the License, or (at your option) any later version.

GROMACS:      gmx mdrun, version 2020.3
Executable:   /home/villa/Programmi/gmx2020.3/bin/gmx
Data prefix:  /home/villa/Programmi/gmx2020.3
Working dir:  /home/villa/tutorials/free-energy/output_files/lambda_01
Process ID:   1807
Command line:
  gmx mdrun

GROMACS version:    2020.3
Verified release checksum is c0599e547549c2d0ef4fc678dc5a26ad0000eab045e938fed756f9ff5b99a197
Precision:          single
Memory model:       64 bit
MPI library:        thread_mpi
OpenMP support:     enabled (GMX_OPENMP_MAX_THREADS = 64)
GPU support:        disabled
SIMD instructions:  AVX_256
FFT library:        fftw-3.3.8-sse2-avx-avx2-avx2_128
RDTSCP usage:       enabled
TNG support:        enabled
Hwloc support:      disabled
Tracing support:    disabled
C compiler:         /usr/bin/cc GNU 7.5.0
C compiler flags:   -mavx -fexcess-precision=fast -funroll-all-loops -fopenmp -O3 -DNDEBUG
C++ compiler:       /usr/bin/c++ GNU 7.5.0
C++ compiler flags: -mavx -fexcess-precision=fast -funroll-all-loops -fopenmp -O3 -DNDEBUG


Running on 1 node with total 2 cores, 4 logical cores
Hardware detected:
  CPU info:
    Vendor: Intel
    Brand:  Intel(R) Core(TM) i5-3317U CPU @ 1.70GHz
    Family: 6   Model: 58   Stepping: 9
    Features: apic avx clfsh cmov cx8 cx16 f16c htt intel lahf mmx msr nonstop_tsc pcid pclmuldq pdcm popcnt pse rdrnd rdtscp sse2 sse3 sse4.1 sse4.2 ssse3 tdt x2apic
  Hardware topology: Basic
    Sockets, cores, and logical processors:
      Socket  0: [   0   1] [   2   3]


++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
M. J. Abraham, T. Murtola, R. Schulz, S. Páll, J. C. Smith, B. Hess, E.
Lindahl
GROMACS: High performance molecular simulations through multi-level
parallelism from laptops to supercomputers
SoftwareX 1 (2015) pp. 19-25
-------- -------- --- Thank You --- -------- --------


++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
S. Páll, M. J. Abraham, C. Kutzner, B. Hess, E. Lindahl
Tackling Exascale Software Challenges in Molecular Dynamics Simulations with
GROMACS
In S. Markidis & E. Laure (Eds.), Solving Software Challenges for Exascale 8759 (2015) pp. 3-27
-------- -------- --- Thank You --- -------- --------


++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
S. Pronk, S. Páll, R. Schulz, P. Larsson, P. Bjelkmar, R. Apostolov, M. R.
Shirts, J. C. Smith, P. M. Kasson, D. van der Spoel, B. Hess, and E. Lindahl
GROMACS 4.5: a high-throughput and highly parallel open source molecular
simulation toolkit
Bioinformatics 29 (2013) pp. 845-54
-------- -------- --- Thank You --- -------- --------


++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
B. Hess and C. Kutzner and D. van der Spoel and E. Lindahl
GROMACS 4: Algorithms for highly efficient, load-balanced, and scalable
molecular simulation
J. Chem. Theory Comput. 4 (2008) pp. 435-447
-------- -------- --- Thank You --- -------- --------


++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
D. van der Spoel, E. Lindahl, B. Hess, G. Groenhof, A. E. Mark and H. J. C.
Berendsen
GROMACS: Fast, Flexible and Free
J. Comp. Chem. 26 (2005) pp. 1701-1719
-------- -------- --- Thank You --- -------- --------


++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
E. Lindahl and B. Hess and D. van der Spoel
GROMACS 3.0: A package for molecular simulation and trajectory analysis
J. Mol. Mod. 7 (2001) pp. 306-317
-------- -------- --- Thank You --- -------- --------


++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
H. J. C. Berendsen, D. van der Spoel and R. van Drunen
GROMACS: A message-passing parallel molecular dynamics implementation
Comp. Phys. Comm. 91 (1995) pp. 43-56
-------- -------- --- Thank You --- -------- --------


++++ PLEASE CITE THE DOI FOR THIS VERSION OF GROMACS ++++
https://doi.org/10.5281/zenodo.3923645
-------- -------- --- Thank You --- -------- --------

Input Parameters:
   integrator                     = sd
   tinit                          = 0
   dt                             = 0.002
   nsteps                         = 100000
   init-step                      = 0
   simulation-part                = 1
   comm-mode                      = Linear
   nstcomm                        = 100
   bd-fric                        = 0
   ld-seed                        = -1192663427
   emtol                          = 10
   emstep                         = 0.01
   niter                          = 20
   fcstep                         = 0
   nstcgsteep                     = 1000
   nbfgscorr                      = 10
   rtpi                           = 0.05
   nstxout                        = 0
   nstvout                        = 0
   nstfout                        = 0
   nstlog                         = 5000
   nstcalcenergy                  = 50
   nstenergy                      = 1000
   nstxout-compressed             = 0
   compressed-x-precision         = 1000
   cutoff-scheme                  = Verlet
   nstlist                        = 10
   pbc                            = xyz
   periodic-molecules             = false
   verlet-buffer-tolerance        = 0.005
   rlist                          = 1.1
   coulombtype                    = PME
   coulomb-modifier               = Potential-shift
   rcoulomb-switch                = 0
   rcoulomb                       = 1.1
   epsilon-r                      = 1
   epsilon-rf                     = inf
   vdw-type                       = Cut-off
   vdw-modifier                   = Potential-shift
   rvdw-switch                    = 0
   rvdw                           = 1.1
   DispCorr                       = No
   table-extension                = 1
   fourierspacing                 = 0.13
   fourier-nx                     = 20
   fourier-ny                     = 20
   fourier-nz                     = 20
   pme-order                      = 4
   ewald-rtol                     = 1e-05
   ewald-rtol-lj                  = 0.001
   lj-pme-comb-rule               = Geometric
   ewald-geometry                 = 0
   epsilon-surface                = 0
   tcoupl                         = No
   nsttcouple                     = -1
   nh-chain-length                = 0
   print-nose-hoover-chain-variables = false
   pcoupl                         = Parrinello-Rahman
   pcoupltype                     = Isotropic
   nstpcouple                     = 10
   tau-p                          = 2
   compressibility (3x3):
      compressibility[    0]={ 4.50000e-05,  0.00000e+00,  0.00000e+00}
      compressibility[    1]={ 0.00000e+00,  4.50000e-05,  0.00000e+00}
      compressibility[    2]={ 0.00000e+00,  0.00000e+00,  4.50000e-05}
   ref-p (3x3):
      ref-p[    0]={ 1.00000e+00,  0.00000e+00,  0.00000e+00}
      ref-p[    1]={ 0.00000e+00,  1.00000e+00,  0.00000e+00}
      ref-p[    2]={ 0.00000e+00,  0.00000e+00,  1.00000e+00}
   refcoord-scaling               = No
   posres-com (3):
      posres-com[0]= 0.00000e+00
      posres-com[1]= 0.00000e+00
      posres-com[2]= 0.00000e+00
   posres-comB (3):
      posres-comB[0]= 0.00000e+00
      posres-comB[1]= 0.00000e+00
      posres-comB[2]= 0.00000e+00
   QMMM                           = false
   QMconstraints                  = 0
   QMMMscheme                     = 0
   MMChargeScaleFactor            = 1
qm-opts:
   ngQM                           = 0
   constraint-algorithm           = Lincs
   continuation                   = false
   Shake-SOR                      = false
   shake-tol                      = 0.0001
   lincs-order                    = 4
   lincs-iter                     = 1
   lincs-warnangle                = 30
   nwall                          = 0
   wall-type                      = 9-3
   wall-r-linpot                  = -1
   wall-atomtype[0]               = -1
   wall-atomtype[1]               = -1
   wall-density[0]                = 0
   wall-density[1]                = 0
   wall-ewald-zfac                = 3
   pull                           = false
   awh                            = false
   rotation                       = false
   interactiveMD                  = false
   disre                          = No
   disre-weighting                = Conservative
   disre-mixed                    = false
   dr-fc                          = 1000
   dr-tau                         = 0
   nstdisreout                    = 100
   orire-fc                       = 0
   orire-tau                      = 0
   nstorireout                    = 100
   free-energy                    = yes
   init-lambda                    = -1
   init-lambda-state              = 1
   delta-lambda                   = 0
   nstdhdl                        = 50
   n-lambdas                      = 7
   separate-dvdl:
       fep-lambdas =   TRUE
      mass-lambdas =   FALSE
      coul-lambdas =   FALSE
       vdw-lambdas =   FALSE
    bonded-lambdas =   FALSE
 restraint-lambdas =   FALSE
temperature-lambdas =   FALSE
all-lambdas:
       fep-lambdas =            0         0.2         0.4         0.6         0.8         0.9           1
      mass-lambdas =            0         0.2         0.4         0.6         0.8         0.9           1
      coul-lambdas =            0         0.2         0.4         0.6         0.8         0.9           1
       vdw-lambdas =            0         0.2         0.4         0.6         0.8         0.9           1
    bonded-lambdas =            0         0.2         0.4         0.6         0.8         0.9           1
 restraint-lambdas =            0         0.2         0.4         0.6         0.8         0.9           1
temperature-lambdas =            0         0.2         0.4         0.6         0.8         0.9           1
   calc-lambda-neighbors          = 1
   dhdl-print-energy              = no
   sc-alpha                       = 1
   sc-power                       = 1
   sc-r-power                     = 6
   sc-sigma                       = 0.3
   sc-sigma-min                   = 0.3
   sc-coul                        = true
   dh-hist-size                   = 0
   dh-hist-spacing                = 0.1
   separate-dhdl-file             = yes
   dhdl-derivatives               = yes
   cos-acceleration               = 0
   deform (3x3):
      deform[    0]={ 0.00000e+00,  0.00000e+00,  0.00000e+00}
      deform[    1]={ 0.00000e+00,  0.00000e+00,  0.00000e+00}
      deform[    2]={ 0.00000e+00,  0.00000e+00,  0.00000e+00}
   simulated-tempering            = false
   swapcoords                     = no
   userint1                       = 0
   userint2                       = 0
   userint3                       = 0
   userint4                       = 0
   userreal1                      = 0
   userreal2                      = 0
   userreal3                      = 0
   userreal4                      = 0
   applied-forces:
     electric-field:
       x:
         E0                       = 0
         omega                    = 0
         t0                       = 0
         sigma                    = 0
       y:
         E0                       = 0
         omega                    = 0
         t0                       = 0
         sigma                    = 0
       z:
         E0                       = 0
         omega                    = 0
         t0                       = 0
         sigma                    = 0
     density-guided-simulation:
       active                     = false
       group                      = protein
       similarity-measure         = inner-product
       atom-spreading-weight      = unity
       force-constant             = 1e+09
       gaussian-transform-spreading-width = 0.2
       gaussian-transform-spreading-range-in-multiples-of-width = 4
       reference-density-filename = reference.mrc
       nst                        = 1
       normalize-densities        = true
       adaptive-force-scaling     = false
       adaptive-force-scaling-time-constant = 4
grpopts:
   nrdf:        1830
   ref-t:         300
   tau-t:           2
annealing:          No
annealing-npoints:           0
   acc:	           0           0           0
   nfreeze:           N           N           N
   energygrp-flags[  0]: 0

Changing nstlist from 10 to 25, rlist from 1.1 to 1.146

Using 1 MPI thread

Non-default thread affinity set, disabling internal thread affinity

Using 4 OpenMP threads 

System total charge, top. A: 0.000 top. B: -0.000
Will do PME sum in reciprocal space for electrostatic interactions.

++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
U. Essmann, L. Perera, M. L. Berkowitz, T. Darden, H. Lee and L. G. Pedersen 
A smooth particle mesh Ewald method
J. Chem. Phys. 103 (1995) pp. 8577-8592
-------- -------- --- Thank You --- -------- --------

Using a Gaussian width (1/beta) of 0.352179 nm for Ewald
Potential shift: LJ r^-12: -3.186e-01 r^-6: -5.645e-01, Ewald -9.091e-06
Initialized non-bonded Ewald tables, spacing: 9.79e-04 size: 1126

Generated table with 1073 data points for 1-4 COUL.
Tabscale = 500 points/nm
Generated table with 1073 data points for 1-4 LJ6.
Tabscale = 500 points/nm
Generated table with 1073 data points for 1-4 LJ12.
Tabscale = 500 points/nm

Using SIMD 4x4 nonbonded short-range kernels

Using a dual 4x4 pair-list setup updated with dynamic pruning:
  outer list: updated every 25 steps, buffer 0.046 nm, rlist 1.146 nm
  inner list: updated every 12 steps, buffer 0.002 nm, rlist 1.102 nm
At tolerance 0.005 kJ/mol/ps per atom, equivalent classical 1x1 list would be:
  outer list: updated every 25 steps, buffer 0.131 nm, rlist 1.231 nm
  inner list: updated every 12 steps, buffer 0.048 nm, rlist 1.148 nm

Using geometric Lennard-Jones combination rule

There are 9 atoms and 9 charges for free energy perturbation
Removing pbc first time

Initializing LINear Constraint Solver

++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
B. Hess and H. Bekker and H. J. C. Berendsen and J. G. E. M. Fraaije
LINCS: A Linear Constraint Solver for molecular simulations
J. Comp. Chem. 18 (1997) pp. 1463-1472
-------- -------- --- Thank You --- -------- --------

The number of constraints is 6

++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
S. Miyamoto and P. A. Kollman
SETTLE: An Analytical Version of the SHAKE and RATTLE Algorithms for Rigid
Water Models
J. Comp. Chem. 13 (1992) pp. 952-962
-------- -------- --- Thank You --- -------- --------

Initial vector of lambda components:[     0.2000     0.2000     0.2000     0.2000     0.2000     0.2000     0.2000 ]

++++ PLEASE READ AND CITE THE FOLLOWING REFERENCE ++++
N. Goga and A. J. Rzepiela and A. H. de Vries and S. J. Marrink and H. J. C.
Berendsen
Efficient Algorithms for Langevin and DPD Dynamics
J. Chem. Theory Comput. 8 (2012) pp. 3637--3649
-------- -------- --- Thank You --- -------- --------

There are: 915 Atoms

Constraining the starting coordinates (step 0)

Constraining the coordinates at t0-dt (step 0)
Center of mass motion removal mode is Linear
We have the following groups for center of mass motion removal:
  0:  rest
RMS relative constraint deviation after constraining: 1.10e-06
Initial temperature: 298.817 K

Started mdrun on rank 0 Fri Aug  7 15:13:43 2020

           Step           Time
              0        0.00000

   Energies (kJ/mol)
           Bond          Angle Ryckaert-Bell.          LJ-14     Coulomb-14
    2.59144e-01    1.14602e+01    8.10414e-01    6.61926e-01   -4.63740e+01
        LJ (SR)   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.
    2.80252e+03   -1.66835e+04    5.41912e+01   -1.38599e+04    2.27361e+03
   Total Energy    Temperature Pressure (bar)    dVremain/dl   Constr. rmsd
   -1.15863e+04    2.98855e+02    6.05409e+02   -6.37122e+01    1.71115e-06

           Step           Time
           5000       10.00000

   Energies (kJ/mol)
           Bond          Angle Ryckaert-Bell.          LJ-14     Coulomb-14
    1.03513e+00    7.97449e+00    1.24321e+00   -1.44451e-01   -4.98061e+01
        LJ (SR)   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.
    2.88499e+03   -1.69547e+04    5.44537e+01   -1.40549e+04    2.31947e+03
   Total Energy    Temperature Pressure (bar)    dVremain/dl   Constr. rmsd
   -1.17354e+04    3.04883e+02    1.02628e+03   -9.58880e+01    1.71943e-06

           Step           Time
          10000       20.00000

   Energies (kJ/mol)
           Bond          Angle Ryckaert-Bell.          LJ-14     Coulomb-14
    7.65460e+00    1.39560e+01    7.62682e-01    3.44621e-01   -4.92968e+01
        LJ (SR)   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.
    2.77690e+03   -1.68123e+04    4.14408e+01   -1.40206e+04    2.33484e+03
   Total Energy    Temperature Pressure (bar)    dVremain/dl   Constr. rmsd
   -1.16857e+04    3.06903e+02    2.56653e+02   -1.07817e+02    2.82478e-06

           Step           Time
          15000       30.00000

   Energies (kJ/mol)
           Bond          Angle Ryckaert-Bell.          LJ-14     Coulomb-14
    5.11873e+00    2.65074e+00   -7.81294e-01    5.62729e-01   -5.14063e+01
        LJ (SR)   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.
    2.75414e+03   -1.69010e+04    5.19813e+01   -1.41388e+04    2.32121e+03
   Total Energy    Temperature Pressure (bar)    dVremain/dl   Constr. rmsd
   -1.18176e+04    3.05111e+02    4.08440e+02   -1.03130e+02    2.79655e-06

           Step           Time
          20000       40.00000

   Energies (kJ/mol)
           Bond          Angle Ryckaert-Bell.          LJ-14     Coulomb-14
    7.89580e-02    8.84056e+00    8.07847e+00    1.12705e+00   -4.90584e+01
        LJ (SR)   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.
    2.68593e+03   -1.65549e+04    4.84916e+01   -1.38514e+04    2.47061e+03
   Total Energy    Temperature Pressure (bar)    dVremain/dl   Constr. rmsd
   -1.13808e+04    3.24750e+02   -5.63690e+01   -7.87292e+01    1.91911e-06

           Step           Time
          25000       50.00000

   Energies (kJ/mol)
           Bond          Angle Ryckaert-Bell.          LJ-14     Coulomb-14
    4.27046e+00    1.27525e+01    9.19986e+00    6.04732e-01   -4.79835e+01
        LJ (SR)   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.
    2.78521e+03   -1.68547e+04    5.27518e+01   -1.40379e+04    2.29085e+03
   Total Energy    Temperature Pressure (bar)    dVremain/dl   Constr. rmsd
   -1.17470e+04    3.01121e+02    4.85807e+02   -9.98965e+01    3.69268e-06

           Step           Time
          30000       60.00000

   Energies (kJ/mol)
           Bond          Angle Ryckaert-Bell.          LJ-14     Coulomb-14
    1.03206e+00    9.16835e+00    3.02468e+00   -7.50475e-03   -4.49973e+01
        LJ (SR)   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.
    2.67067e+03   -1.66368e+04    4.66201e+01   -1.39513e+04    2.33802e+03
   Total Energy    Temperature Pressure (bar)    dVremain/dl   Constr. rmsd
   -1.16132e+04    3.07321e+02   -1.29247e+02   -7.46379e+01    1.39085e-06

           Step           Time
          35000       70.00000

   Energies (kJ/mol)
           Bond          Angle Ryckaert-Bell.          LJ-14     Coulomb-14
    4.88133e+00    1.62706e+01    5.06643e+00   -3.37621e-01   -4.40341e+01
        LJ (SR)   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.
    3.07871e+03   -1.72486e+04    5.33134e+01   -1.41348e+04    2.18564e+03
   Total Energy    Temperature Pressure (bar)    dVremain/dl   Constr. rmsd
   -1.19491e+04    2.87292e+02    1.36576e+03   -9.62862e+01    6.80033e-07

           Step           Time
          40000       80.00000

   Energies (kJ/mol)
           Bond          Angle Ryckaert-Bell.          LJ-14     Coulomb-14
    2.87382e+00    3.52685e+00   -5.55078e-01    9.49978e-01   -5.14875e+01
        LJ (SR)   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.
    2.57587e+03   -1.65673e+04    4.69980e+01   -1.39891e+04    2.32350e+03
   Total Energy    Temperature Pressure (bar)    dVremain/dl   Constr. rmsd
   -1.16656e+04    3.05413e+02   -6.38605e+02   -9.77138e+01    8.17982e-07

           Step           Time
          45000       90.00000

   Energies (kJ/mol)
           Bond          Angle Ryckaert-Bell.          LJ-14     Coulomb-14
    5.13262e+00    1.31748e+01    4.97236e-01    1.47366e+00   -4.74766e+01
        LJ (SR)   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.
    2.75159e+03   -1.67878e+04    5.08246e+01   -1.40126e+04    2.33573e+03
   Total Energy    Temperature Pressure (bar)    dVremain/dl   Constr. rmsd
   -1.16769e+04    3.07021e+02    2.81171e+02   -9.55488e+01    3.42472e-06

           Step           Time
          50000      100.00000

   Energies (kJ/mol)
           Bond          Angle Ryckaert-Bell.          LJ-14     Coulomb-14
    1.85225e+00    1.31914e+01    2.72272e+00    6.72183e-01   -4.66683e+01
        LJ (SR)   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.
    2.58659e+03   -1.64736e+04    5.54817e+01   -1.38597e+04    2.38033e+03
   Total Energy    Temperature Pressure (bar)    dVremain/dl   Constr. rmsd
   -1.14794e+04    3.12883e+02   -3.35765e+02   -9.50066e+01    3.16953e-06

           Step           Time
          55000      110.00000

   Energies (kJ/mol)
           Bond          Angle Ryckaert-Bell.          LJ-14     Coulomb-14
    3.99319e+00    2.28424e+01   -9.41683e-01    1.59207e+00   -5.13389e+01
        LJ (SR)   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.
    2.72325e+03   -1.68370e+04    5.52713e+01   -1.40823e+04    2.32590e+03
   Total Energy    Temperature Pressure (bar)    dVremain/dl   Constr. rmsd
   -1.17564e+04    3.05728e+02   -5.55479e+01   -9.35710e+01    2.62599e-06

           Step           Time
          60000      120.00000

   Energies (kJ/mol)
           Bond          Angle Ryckaert-Bell.          LJ-14     Coulomb-14
    2.26478e-01    1.40552e+01   -3.71586e-01   -2.02333e-02   -4.85045e+01
        LJ (SR)   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.
    2.81695e+03   -1.68399e+04    4.79338e+01   -1.40096e+04    2.18285e+03
   Total Energy    Temperature Pressure (bar)    dVremain/dl   Constr. rmsd
   -1.18268e+04    2.86925e+02    2.46130e+02   -1.07687e+02    1.24672e-06

           Step           Time
          65000      130.00000

   Energies (kJ/mol)
           Bond          Angle Ryckaert-Bell.          LJ-14     Coulomb-14
    1.75876e-01    7.87722e+00   -1.92472e-01    2.59794e-01   -4.93867e+01
        LJ (SR)   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.
    2.88642e+03   -1.70629e+04    4.51357e+01   -1.41726e+04    2.20874e+03
   Total Energy    Temperature Pressure (bar)    dVremain/dl   Constr. rmsd
   -1.19638e+04    2.90329e+02    3.44640e+02   -9.55963e+01    1.35308e-06

           Step           Time
          70000      140.00000

   Energies (kJ/mol)
           Bond          Angle Ryckaert-Bell.          LJ-14     Coulomb-14
    1.88252e+00    9.17659e+00    1.47113e+00   -3.67736e-02   -4.46833e+01
        LJ (SR)   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.
    2.63210e+03   -1.65699e+04    4.38434e+01   -1.39261e+04    2.12506e+03
   Total Energy    Temperature Pressure (bar)    dVremain/dl   Constr. rmsd
   -1.18011e+04    2.79329e+02   -8.16913e+02   -8.65956e+01    1.20886e-06

           Step           Time
          75000      150.00000

   Energies (kJ/mol)
           Bond          Angle Ryckaert-Bell.          LJ-14     Coulomb-14
    5.63006e-01    8.77032e+00    3.46464e-01    1.34490e-01   -4.52296e+01
        LJ (SR)   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.
    2.81457e+03   -1.69295e+04    4.53959e+01   -1.41049e+04    2.17611e+03
   Total Energy    Temperature Pressure (bar)    dVremain/dl   Constr. rmsd
   -1.19288e+04    2.86039e+02    4.68953e-03   -9.82079e+01    3.68434e-06

           Step           Time
          80000      160.00000

   Energies (kJ/mol)
           Bond          Angle Ryckaert-Bell.          LJ-14     Coulomb-14
    1.53622e+00    1.66887e+01    2.37731e+00    7.12261e-01   -4.80848e+01
        LJ (SR)   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.
    2.71596e+03   -1.68557e+04    4.43152e+01   -1.41222e+04    2.34040e+03
   Total Energy    Temperature Pressure (bar)    dVremain/dl   Constr. rmsd
   -1.17818e+04    3.07634e+02   -3.16295e+02   -8.92089e+01    8.72620e-07

           Step           Time
          85000      170.00000

   Energies (kJ/mol)
           Bond          Angle Ryckaert-Bell.          LJ-14     Coulomb-14
    1.56295e+01    1.20682e+01    3.60061e+00    5.50494e-02   -4.46740e+01
        LJ (SR)   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.
    2.45310e+03   -1.62752e+04    5.64994e+01   -1.37789e+04    2.18691e+03
   Total Energy    Temperature Pressure (bar)    dVremain/dl   Constr. rmsd
   -1.15920e+04    2.87459e+02   -1.27972e+03   -9.29204e+01    3.21165e-06

           Step           Time
          90000      180.00000

   Energies (kJ/mol)
           Bond          Angle Ryckaert-Bell.          LJ-14     Coulomb-14
    1.05685e+01    7.73703e+00    7.68254e+00   -6.59332e-02   -4.44993e+01
        LJ (SR)   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.
    2.55871e+03   -1.64938e+04    5.49008e+01   -1.38987e+04    2.25567e+03
   Total Energy    Temperature Pressure (bar)    dVremain/dl   Constr. rmsd
   -1.16431e+04    2.96497e+02   -1.51363e+03   -9.95417e+01    7.41537e-06

           Step           Time
          95000      190.00000

   Energies (kJ/mol)
           Bond          Angle Ryckaert-Bell.          LJ-14     Coulomb-14
    8.97785e-01    8.30394e+00    3.89320e+00    4.75346e-01   -4.61654e+01
        LJ (SR)   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.
    2.91635e+03   -1.68334e+04    5.81658e+01   -1.38915e+04    2.21307e+03
   Total Energy    Temperature Pressure (bar)    dVremain/dl   Constr. rmsd
   -1.16784e+04    2.90897e+02    1.54102e+03   -9.37450e+01    2.38890e-06

           Step           Time
         100000      200.00000

Writing checkpoint, step 100000 at Fri Aug  7 15:16:37 2020


   Energies (kJ/mol)
           Bond          Angle Ryckaert-Bell.          LJ-14     Coulomb-14
    1.90219e+00    1.19124e+01   -7.39172e-01    5.63074e-01   -5.12631e+01
        LJ (SR)   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.
    2.62457e+03   -1.66810e+04    5.05594e+01   -1.40435e+04    2.20783e+03
   Total Energy    Temperature Pressure (bar)    dVremain/dl   Constr. rmsd
   -1.18357e+04    2.90208e+02   -2.40671e+02   -9.38697e+01    2.98023e-07


	<======  ###############  ==>
	<====  A V E R A G E S  ====>
	<==  ###############  ======>

	Statistics over 100001 steps using 2001 frames

   Energies (kJ/mol)
           Bond          Angle Ryckaert-Bell.          LJ-14     Coulomb-14
    2.55773e+00    1.32685e+01    1.76371e+00    5.60523e-01   -4.78437e+01
        LJ (SR)   Coulomb (SR)   Coul. recip.      Potential    Kinetic En.
    2.73050e+03   -1.67621e+04    5.12627e+01   -1.40100e+04    2.28962e+03
   Total Energy    Temperature Pressure (bar)    dVremain/dl   Constr. rmsd
   -1.17204e+04    3.00959e+02   -1.38694e+01   -9.05282e+01    0.00000e+00

          Box-X          Box-Y          Box-Z
    2.34498e+00    2.34498e+00    1.65817e+00

   Total Virial (kJ/mol)
    7.74515e+02   -3.79600e+00    1.69404e+01
   -3.79394e+00    7.60871e+02    4.33488e-01
    1.69341e+01    4.29516e-01    7.70242e+02

   Pressure (bar)
   -3.91762e+01    1.21767e+01   -6.56606e+01
    1.21691e+01    1.71750e+01    8.74086e+00
   -6.56376e+01    8.75525e+00   -1.96069e+01


	M E G A - F L O P S   A C C O U N T I N G

 NB=Group-cutoff nonbonded kernels    NxN=N-by-N cluster Verlet kernels
 RF=Reaction-Field  VdW=Van der Waals  QSTab=quadratic-spline table
 W3=SPC/TIP3p  W4=TIP4p (single or pairs)
 V&F=Potential and force  V=Potential only  F=Force only

 Computing:                               M-Number         M-Flops  % Flops
-----------------------------------------------------------------------------
 NB Free energy kernel               161413.244322      161413.244     4.0
 Pair Search distance check            1320.330930       11882.978     0.3
 NxN Ewald Elec. + LJ [F]             26448.708168     1745614.739    43.1
 NxN Ewald Elec. + LJ [V&F]             539.948944       57774.537     1.4
 NxN Ewald Elec. [F]                  25652.652920     1564811.828    38.6
 NxN Ewald Elec. [V&F]                  523.780816       43997.589     1.1
 1,4 nonbonded interactions               0.700007          63.001     0.0
 Calc Weights                           274.502745        9882.099     0.2
 Spread Q Bspline                     11712.117120       23424.234     0.6
 Gather F Bspline                     11712.117120       70272.703     1.7
 3D-FFT                               41490.814904      331926.519     8.2
 Solve PME                               80.000800        5120.051     0.1
 Shift-X                                  3.660915          21.965     0.0
 Bonds                                    0.200002          11.800     0.0
 Angles                                   1.300013         218.402     0.0
 RB-Dihedrals                             1.200012         296.403     0.0
 Virial                                   9.600960         172.817     0.0
 Update                                  91.500915        2836.528     0.1
 Stop-CM                                  0.916830           9.168     0.0
 Calc-Ekin                               36.601830         988.249     0.0
 Lincs                                    1.200024          72.001     0.0
 Lincs-Mat                                9.600192          38.401     0.0
 Constraint-V                           183.602754        1468.822     0.0
 Constraint-Vir                           9.120912         218.902     0.0
 Settle                                  60.401208       19509.590     0.5
-----------------------------------------------------------------------------
 Total                                                 4052046.573   100.0
-----------------------------------------------------------------------------


     R E A L   C Y C L E   A N D   T I M E   A C C O U N T I N G

On 1 MPI rank, each using 4 OpenMP threads

 Computing:          Num   Num      Call    Wall time         Giga-Cycles
                     Ranks Threads  Count      (s)         total sum    %
-----------------------------------------------------------------------------
 Neighbor search        1    4       4001       6.218         42.183   3.6
 Force                  1    4     100001     126.345        857.179  72.7
 PME mesh               1    4     100001      31.420        213.166  18.1
 NB X/F buffer ops.     1    4     196001       2.294         15.564   1.3
 Write traj.            1    4          1       0.026          0.178   0.0
 Update                 1    4     200002       3.458         23.458   2.0
 Constraints            1    4     200004       2.681         18.188   1.5
 Rest                                           1.432          9.712   0.8
-----------------------------------------------------------------------------
 Total                                        173.873       1179.629 100.0
-----------------------------------------------------------------------------
 Breakdown of PME mesh computation
-----------------------------------------------------------------------------
 PME spread             1    4     200002       8.506         57.706   4.9
 PME gather             1    4     200002       9.248         62.741   5.3
 PME 3D-FFT             1    4     400004       9.770         66.284   5.6
 PME solve Elec         1    4     200002       3.434         23.301   2.0
-----------------------------------------------------------------------------

               Core t (s)   Wall t (s)        (%)
       Time:      695.491      173.873      400.0
                 (ns/day)    (hour/ns)
Performance:       99.384        0.241
Finished mdrun on rank 0 Fri Aug  7 15:16:37 2020

