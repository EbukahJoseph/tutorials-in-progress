title                    = AWH-multi simulation 
integrator               = md
dt                       = 0.002
nsteps                   = 5000000

nstlog                   = 5000000
nstenergy                = 50000
nstxout-compressed       = 500000

; Settings that make sure we run with parameters in harmony with the selected force-field
coulombtype              = pme
rcoulomb                 = 1.0
fourierspacing           = 0.121 ; grid spacing for FFT
;
vdwtype                  = cut-off
vdw-modifier             = potential-shift-verlet
rvdw                     = 1.0
dispCorr                 = enerpres
constraints              = h-bonds

; Pressure coupling is on
pcoupl                   = parrinello-rahman  
pcoupl-type              = semiisotropic  ; isotropic in the x and y direction, but different in the z direction  
tau_p                    = 5.0 
ref_p                    = 1.0 1.0
compressibility          = 4.5e-5 4.5e-5 

; Keep system temperature fluctuating physically correct
tcoupl                   = v-rescale 
tc-grps                  = system
tau_t                    = 0.5 
ref_t                    = 300 

gen-vel                  = no
gen-temp                 = 300
gen-seed                 = -1


periodic-molecules       = yes              ; for systems with molecules that couple to themselves through the periodic boundary conditions, this requires a slower PBC algorithm and molecules are not made whole in the output.

pull                     = yes                 ; The reaction coordinate (RC) is defined using pull coordinates.
pull-ngroups             = 2                   ; The number of atom groups needed to define the pull coordinate.
pull-ncoords             = 1                   ; Number of pull coordinates.
pull-nstxout             = 50                 ; Step interval to output the coordinate values to the pullx.xvg.
pull-nstfout             = 0                   ; Step interval to output the applied force (skip here).
					       
pull-group1-name         = base_N1orN3         ; Name of pull group 1 corresponding to an entry in an index file.
pull-group2-name         = partner_N1orN3      ; Same, but for group 2.
					       
pull-coord1-groups       = 1 2                 ; Which groups define coordinate 1? Here, groups 1 and 2.
pull-coord1-geometry     = distance            ; How is the coordinate defined? Here by the COM distance.
pull-coord1-type         = external-potential  ; Apply the bias using an external module.
pull-coord1-potential-provider = AWH           ; The external module is called AWH!

awh                      = yes                 ; AWH on.
awh-nstout               = 50000               ; Step interval for writing awh*.xvg files.
awh-nbias                = 1                   ; One bias, could have multiple.
awh1-ndim                = 1                   ; Dimensionality of the RC, each dimension per pull coordinate.
awh1-dim1-coord-index    = 1                   ; Map RC dimension to pull coordinate index (here 1-->1)
awh1-dim1-start          = 0.25                ; Sampling interval min value (nm)
awh1-dim1-end            = 0.65                ; Sampling interval max value (nm)
awh1-dim1-force-constant = 128000              ; Force constant of the harmonic potential (kJ/(mol*nm^2))
awh1-dim1-diffusion      = 5e-5                ; Estimate of the diffusion (nm^2/ps)
awh-share-multisim       = yes                 ; Share bias across simulations
awh1-share-group         = 1                   ; Non-zero share group index
awh1-equilibrate-histogram = yes               ; Ensure histogram is close to target before decreasing the update size
