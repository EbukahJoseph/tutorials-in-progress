{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Fast Growth TI - Calculating the relative free energy of binding for two different ligands\n",
    "\n",
    "```\n",
    "Author        : Christian Blau \n",
    "Goal          : Learn how calculate the relative free energy of binding \n",
    "                of two different ligands to the same protein \n",
    "                using fast-growth and Crooks fluctuation theorem\n",
    "Time          : 90 minutes \n",
    "Prerequisites : basic molecular dynamics simulation\n",
    "                hybrid topology tutorial\n",
    "                free energy overview tutorial\n",
    "References    :\n",
    "Software      : GROMACS 2020.1\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you have not done so yet, we recommend having a look at the free energy methods overview [tutorial](overview.ipynb)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Relative free energies from fast transitions<a class=\"anchor\" id=\"fast-transitions\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this tutorial, we calculate the free energy differences using the following considerations\n",
    "\n",
    " * the difference in free energy is linked to the work that is required to morph ethanol to propanol (A$\\rightarrow$B)\n",
    " * the work required to morph ethanol to propanol will depend on \n",
    "   * the ethanol pose when we started the morph, thus **we will have to perform many morphs from a statistically representative pool of ethanol configurations and are rather interested in the work propability distribution P(W) of morphs rather than a single work value**\n",
    "   * how fast we perform the \"morph\"\n",
    " * the setup is symmetric, we can also morph propanol to ethanol (B$\\rightarrow$A, \"reverse\" direction)\n",
    " * the work distributions for \"forward\" and \"reverse\" morphing will differ, if we don't morph infinitely slowly,  but they are linked via Crooks-Fluctuation theorem"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "insert image here, showing differnt ethanol poses and morphs to propanol and vice-versa, together with work distribution."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The single equation that determines all our setup is Crooks fluctuation theorem,\n",
    "\n",
    "$$P_{\\mathrm{A}\\rightarrow\\mathrm{B}}(W) = P_{\\mathrm{B}\\rightarrow\\mathrm{A}}(W) \\exp\\left(\\cfrac{W-\\Delta G}{k_{\\mathrm{B}}T}\\right)$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At the end of the tutorial we will explore the best practices to extract $\\Delta G$ from a forward and backward work distribution, using this equation that also include error estimates. To get an feeling, for how this is possible, consider what this equation looks like at the point where the two work distributions cross each other, so that $P_{\\mathrm{A}\\rightarrow\\mathrm{B}}(W) = P_{\\mathrm{B}\\rightarrow\\mathrm{A}}(W)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Simulating transitions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remember the simulation setup for the fast-growth method from the overview [tutorial](overview.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![CFT](images/fepmethods-cft.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We start with sampling an equilibrium distribution of states two long simulations - ethanol and 1-propanol in water. Then we pick starting frames to perform the fast-growth transitions. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Equilibrium simulation setup for ethanol "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Simulation box setup"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we put ethanol in a reasonably sized simulation box. Note that the molecule is automatically centered in the simulation box."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "gmx editconf -bt dodecahedron -d 1.5 -f mergedA.pdb -o ethanol-box.gro"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the next step, we add water to the system and update the `topol.top` file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "gmx solvate -p topol.top -cp ethanol-box.gro -cs spc216.gro -o ethanol-solvated.gro"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before we can go ahead with grompp, we have to explicitely include \"tip3p\" water in the topology. We add that line."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we generate a `tpr` file for placing the ions. This step that is required for genion to work has the useful side-effect here, that it checks if we have all the necessary parameters for grompp."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "touch empty.mdp # just a temporary\n",
    "gmx grompp -f empty.mdp -c ethanol-solvated.gro\n",
    "rm empty.mdp # remove this"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we're ready to add ions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "echo \"SOL\"|gmx genion -o ethanol-solvated-ions.gro -p topol.top -pname NA -nname CL -conc 0.15"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Energy minimization of ligand in water "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's prepare the input to relax the ligand structure with respect to the current force-field."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We use a vanilla .mdp eneregy minimisation file suited for amber and run until finished (nsteps = -1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "cat input/simulation-settings/emin-amber.mdp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Preparing the input"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "gmx grompp -o ethanol-minimization.tpr -f input/simulation-settings/emin-amber.mdp \\\n",
    " -c ethanol-solvated-ions.gro"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Running the energy minimization, which should be around a hundred very quick steps."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "gmx mdrun -s ethanol-minimization.tpr -c ethanol-solvated-minimized.pdb"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In protein systems, we might want to run some other careful equilibration steps, that, e.g., carefully release heavy atoms and backbone. For a system like ethanol, we can go ahead directly with an MD-simulation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### MD simulation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run a \"normal\" MD simulation to generate an ensemble of structures, however with a few modifications.\n",
    "\n",
    " * use temperature coupling on the whole system, since we don't have any protein in here yet and the ligand is too small to be it's own temperature coupling group\n",
    " * output the uncompressed coordinates and velocities every 10 ps, because we want to continue at the very exact point in phase space for the transitioning simulations. Due to the high friction in the systems, an approximative approach that only saves the coordinates will work as well, but requires an extra step in heating up the system when transitioning later.\n",
    " \n",
    "Let's change a default `.mdp` file and have a look at the input parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "sed 's/tc-grps                 = Protein Non-Protein/tc-grps = system/' input/simulation-settings/md-amber.mdp |\n",
    "sed 's/tau_t                   = 0.1     0.1/tau_t = 0.1/' |\n",
    "sed 's/ref_t                   = 300     300/ref_t = 300/' |\n",
    "sed '9instxout                 = 5000      ; 2 * 5000 = 10ps'    |\n",
    "sed '9instvout                 = 5000      ; 2 * 5000 = 10ps'    > md-ethanol.mdp\n",
    "cat md-ethanol.mdp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we prepare the system following a standard procedure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "gmx grompp -o md-ethanol.tpr -f md-ethanol.mdp -c ethanol-solvated-minimized.pdb"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we are ready to run the simulation which should take about three minutes or less. If you want to skip to run the simulation, copy a pre-calculated one from `reference/ethanol.trr`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "gmx mdrun -v -s md-ethanol.tpr -o ethanol.trr -x ethanol.xtc"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Equilibrium simulation setup for 1-propanol"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To keep the distortion to the thermodynamic cycle as small as possible, we use the same water and ion setup as in the ethanol simulation. The change in ligand is so small that we \"swap\" out ethanol for propanol in the fully solvated ethanol with ions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Create index groups to align the propanol molecule to ethanol in solution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "printf \"[ common-atoms ]\\n 1 2 3\\n\\n[ propanol ]\\n 1 2 3 4 5 6 7 8 9 10 11 12\\n\" > ethanol-propanol-common-index.ndx\n",
    "cat ethanol-propanol-common-index.ndx"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's align 1-propanol to ethanol on a common subset of atoms.\n",
    "\n",
    "*Note that this procedure also works without this alignment step, but keeps pertubation to the system smaller*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "printf \"common-atoms\\ncommon-atoms\\n\" | \n",
    "gmx confrms -nomw  -one -f1 md-ethanol.tpr \\\n",
    "    -n1 ethanol-propanol-common-index.ndx\\\n",
    "    -n2 ethanol-propanol-common-index.ndx\\\n",
    "    -f2 mergedB.pdb \\\n",
    "    -o propanol-fit.pdb"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Swap out ethanol for propanol in the system with water and ions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "grep CRYST ethanol-solvated-minimized.pdb > propanol-solvated.pdb \n",
    "cat propanol-fit.pdb | grep 'ATOM' >> propanol-solvated.pdb\n",
    "grep -v LIG ethanol-solvated-minimized.pdb | grep 'ATOM' >> propanol-solvated.pdb"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we want to run an energy minimisation, but with the molecules described in topology acting as propanol. To this goal, we have to activate the B-state in the topology with the free energy options in the `.mdp` files. \n",
    "\n",
    "**NOTE An alternative to this approach is to generate a new topology purely for propanol and foregoes the change in free energy parameter setting (e.g. by swapping A- and B-state in the topology). Running simulations with the free energy calculation machinery slows down simulations, thus for larger systems, it is preferable to run without free-energy activated if possible.**\n",
    "\n",
    "The settings for this are\n",
    "```bash\n",
    "free-energy    = yes\n",
    "init-lambda    = 1\n",
    "delta-lambda   = 0\n",
    "```\n",
    "\n",
    "All free energy related `.mdp` options are documented in the gromacs manual chapter on [mdp options](http://manual.gromacs.org/documentation/current/user-guide/mdp-options.html#free-energy-calculations)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let's add these parameters to a default `.mdp` input file for energy minimisation in amber:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "sed '1ifree-energy             = yes' input/simulation-settings/emin-amber.mdp |\n",
    "sed '1iinit-lambda             = 1' |\n",
    "sed '1idelta-lambda            = 0' > emin-propanol.mdp"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "gmx grompp -p topol.top -o propanol-minimization.tpr -f emin-propanol.mdp\\\n",
    "    -c propanol-solvated.pdb\n",
    "gmx mdrun -s propanol-minimization.tpr -c propanol-solvated-minimized.gro"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's do the same additions to the md run options as above, making sure that we have the right temperature coupling, output sufficient information for a smooth continuation and make sure we use the *B* state. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "sed 's/tc-grps                 = Protein Non-Protein/tc-grps = system/' input/simulation-settings/md-amber.mdp |\n",
    "sed 's/tau_t                   = 0.1     0.1/tau_t = 0.1/' |\n",
    "sed 's/ref_t                   = 300     300/ref_t = 300/' |\n",
    "sed '9instxout                 = 5000      ; 2 * 5000 = 10ps'    |\n",
    "sed '9instvout                 = 5000      ; 2 * 5000 = 10ps'    |\n",
    "sed '1ifree-energy             = yes' |\n",
    "sed '2iinit-lambda             = 1' |\n",
    "sed '3idelta-lambda            = 0' > md-propanol.mdp\n",
    "cat md-propanol.mdp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now it's time to compile all into a run input file and then run the actual simulation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "gmx grompp -p topol.top -o md-propanol.tpr -f md-propanol.mdp -c propanol-solvated-minimized.gro"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This simulation again will run in three minutes or less. If you want to avoid running for that long, you can use the data provided in `reference/propanol.trr` by copying this to the current directory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "gmx mdrun -v -s md-propanol.tpr -o propanol.trr -x propanol.xtc "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Visualize solvated system"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "printf \"System\\n\" | gmx trjconv -pbc atom -ur compact \\\n",
    "-s reference/ethanol-solvated-minimized.gro \\\n",
    "-f reference/ethanol-solvated-minimized.gro \\\n",
    "-o ethanol-compact.gro\n",
    "\n",
    "printf \"System\\n\" | gmx trjconv -pbc atom -ur compact \\\n",
    "-s reference/propanol-solvated-minimized.gro \\\n",
    "-f reference/propanol-solvated-minimized.gro \\\n",
    "-o propanol-compact.gro"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Picking a representative ensemble of frames "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's split the trajectory into intervalls of equal length. Here, we want to take hold of all the system, as well as positions and velocities which are stored in the `.trr` file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 221,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Select group for output\n",
      "Selected 0: 'System'\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "                     :-) GROMACS - gmx trjconv, 2018.1 (-:\n",
      "\n",
      "                            GROMACS is written by:\n",
      "     Emile Apol      Rossen Apostolov      Paul Bauer     Herman J.C. Berendsen\n",
      "    Par Bjelkmar    Aldert van Buuren   Rudi van Drunen     Anton Feenstra  \n",
      "  Gerrit Groenhof    Aleksei Iupinov   Christoph Junghans   Anca Hamuraru   \n",
      " Vincent Hindriksen Dimitrios Karkoulis    Peter Kasson        Jiri Kraus    \n",
      "  Carsten Kutzner      Per Larsson      Justin A. Lemkul    Viveca Lindahl  \n",
      "  Magnus Lundborg   Pieter Meulenhoff    Erik Marklund      Teemu Murtola   \n",
      "    Szilard Pall       Sander Pronk      Roland Schulz     Alexey Shvetsov  \n",
      "   Michael Shirts     Alfons Sijbers     Peter Tieleman    Teemu Virolainen \n",
      " Christian Wennberg    Maarten Wolf   \n",
      "                           and the project leaders:\n",
      "        Mark Abraham, Berk Hess, Erik Lindahl, and David van der Spoel\n",
      "\n",
      "Copyright (c) 1991-2000, University of Groningen, The Netherlands.\n",
      "Copyright (c) 2001-2017, The GROMACS development team at\n",
      "Uppsala University, Stockholm University and\n",
      "the Royal Institute of Technology, Sweden.\n",
      "check out http://www.gromacs.org for more information.\n",
      "\n",
      "GROMACS is free software; you can redistribute it and/or modify it\n",
      "under the terms of the GNU Lesser General Public License\n",
      "as published by the Free Software Foundation; either version 2.1\n",
      "of the License, or (at your option) any later version.\n",
      "\n",
      "GROMACS:      gmx trjconv, version 2018.1\n",
      "Executable:   /usr/bin/gmx\n",
      "Data prefix:  /usr\n",
      "Working dir:  /home/cblau/Projects/tutorials/fast-growth-ti/data\n",
      "Command line:\n",
      "  gmx trjconv -f ethanol.trr -s md-ethanol.tpr -split 10 -o ethanol-split.gro\n",
      "\n",
      "Will write gro: Coordinate file in Gromos-87 format\n",
      "Reading file md-ethanol.tpr, VERSION 2018.1 (single precision)\n",
      "Reading file md-ethanol.tpr, VERSION 2018.1 (single precision)\n",
      "Group     0 (         System) has  2913 elements\n",
      "Group     1 (          Other) has    12 elements\n",
      "Group     2 (            LIG) has    12 elements\n",
      "Group     3 (             NA) has     3 elements\n",
      "Group     4 (             CL) has     3 elements\n",
      "Group     5 (          Water) has  2895 elements\n",
      "Group     6 (            SOL) has  2895 elements\n",
      "Group     7 (      non-Water) has    18 elements\n",
      "Group     8 (            Ion) has     6 elements\n",
      "Group     9 (            LIG) has    12 elements\n",
      "Group    10 (             NA) has     3 elements\n",
      "Group    11 (             CL) has     3 elements\n",
      "Group    12 ( Water_and_ions) has  2901 elements\n",
      "Select a group: trr version: GMX_trn_file (single precision)\n",
      "\r",
      "Reading frame       0 time    0.000   \n",
      "Setting output precision to 0.001 (nm)\n",
      " ->  frame      0 time    0.000      \r\n",
      "Back Off! I just backed up ethanol-split0.gro to ./#ethanol-split0.gro.1#\n",
      "\r",
      "Reading frame       1 time   10.000    ->  frame      1 time   10.000      \r\n",
      "Back Off! I just backed up ethanol-split1.gro to ./#ethanol-split1.gro.1#\n",
      "\r",
      "Reading frame       2 time   20.000    ->  frame      2 time   20.000      \r\n",
      "Back Off! I just backed up ethanol-split2.gro to ./#ethanol-split2.gro.1#\n",
      "\r",
      "Reading frame       3 time   30.000    ->  frame      3 time   30.000      \r\n",
      "Back Off! I just backed up ethanol-split3.gro to ./#ethanol-split3.gro.1#\n",
      "\r",
      "Reading frame       4 time   40.000    ->  frame      4 time   40.000      \r\n",
      "Back Off! I just backed up ethanol-split4.gro to ./#ethanol-split4.gro.1#\n",
      "\r",
      "Reading frame       5 time   50.000    ->  frame      5 time   50.000      \r\n",
      "Back Off! I just backed up ethanol-split5.gro to ./#ethanol-split5.gro.1#\n",
      "\r",
      "Reading frame       6 time   60.000    ->  frame      6 time   60.000      \r\n",
      "Back Off! I just backed up ethanol-split6.gro to ./#ethanol-split6.gro.1#\n",
      "\r",
      "Reading frame       7 time   70.000    ->  frame      7 time   70.000      \r\n",
      "Back Off! I just backed up ethanol-split7.gro to ./#ethanol-split7.gro.1#\n",
      "\r",
      "Reading frame       8 time   80.000    ->  frame      8 time   80.000      \r\n",
      "Back Off! I just backed up ethanol-split8.gro to ./#ethanol-split8.gro.1#\n",
      "\r",
      "Reading frame       9 time   90.000    ->  frame      9 time   90.000      \r\n",
      "Back Off! I just backed up ethanol-split9.gro to ./#ethanol-split9.gro.1#\n",
      "\r",
      "Reading frame      10 time  100.000    ->  frame     10 time  100.000      \r\n",
      "Back Off! I just backed up ethanol-split10.gro to ./#ethanol-split10.gro.1#\n",
      "\r",
      "Last frame         10 time  100.000   \n",
      "\n",
      "\n",
      "GROMACS reminds you: \"I was told I'd never make it to VP rank because I was too outspoken. Maybe so, but I think men will always find an excuse for keeping women in their 'place.' So, let's make that place the executive suite and start more of our own companies.\" (Jean Bartik, ENIAC developer)\n",
      "\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "echo \"System\" |gmx trjconv -f ethanol.trr -s md-ethanol.tpr -split 10 -o ethanol-split.gro"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We set up the \"fast-growth\" simulation during which we'll keep track of the work required to change the system from one state to another."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 222,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "free-energy             = yes\n",
      "init-lambda             = 0\n",
      "delta-lambda            = 4e-5 ; we want delta-lambda * nsteps = 1\n",
      "fep-lambdas            = 0.0 0.0 0.0 0.0 0.4 0.5 0.6 0.7 0.8 1.0\n",
      "coul-lambdas           = 0.0 0.2 0.5 1.0 1.0 1.0 1.0 1.0 1.0 1.0\n",
      "restraint-lambdas      = 0.0 0.0 0.1 0.2 0.3 0.5 0.7 1.0 1.0 1.0\n",
      "nsteps                  = 25000  ; this has to match detla-lambda\n",
      "sc-alpha                = 0.3\n",
      "sc-sigma                = 0.25\n",
      "sc-power                = 1\n",
      "continuation            = yes\n",
      "\n",
      "sc-coul                 = yes\n",
      "nstcalcenergy           = 1\n",
      "nstdhdl                 = 1\n",
      "title                   = Amber NPT \n",
      "\n",
      "; Parameters describing what to do, when to stop and what to save\n",
      "integrator              = md        ; leap-frog integrator\n",
      "dt                      = 0.002     ; 2 fs\n",
      "nstenergy               = 1000      ; save energy and temperature every 2ps\n",
      "nstxout-compressed      = 1000      ; save every 2ps\n",
      "\n",
      "; Pressure coupling is on\n",
      "pcoupl                  = Parrinello-Rahman     ; Pressure coupling on in NPT\n",
      "pcoupltype              = isotropic             ; uniform scaling of box vectors\n",
      "tau_p                   = 2.0                   ; time constant, in ps\n",
      "ref_p                   = 1.0                   ; reference pressure, in bar\n",
      "compressibility         = 4.5e-5                ; isothermal compressibility of water, bar^-1\n",
      "refcoord_scaling        = com\n",
      "\n",
      "; Keep system temperature fluctuating physically correct\n",
      "tcoupl                  = V-rescale           ; modified Berendsen thermostat\n",
      "tc-grps = system ; two coupling groups - more accurate\n",
      "tau_t = 0.1         ; time constant, in ps\n",
      "ref_t = 300         ; reference temperature, one for each group, in K\n",
      "\n",
      "; Settings that make sure we run with parameters in harmony with the selected force-field\n",
      "constraints             = h-bonds   ; bonds involving H are constrained\n",
      "rcoulomb                = 1.0       ; short-range electrostatic cutoff (in nm)\n",
      "rvdw                    = 1.0       ; short-range van der Waals cutoff (in nm)\n",
      "vdw-modifier            = Potential-shift-Verlet ; Amber specific\n",
      "DispCorr                = EnerPres  ; account for cut-off vdW scheme\n",
      "coulombtype             = PME       ; Particle Mesh Ewald for long-range electrostatics\n",
      "fourierspacing          = 0.125     ; grid spacing for FFT\n",
      "\n",
      "; Setting to avoid grompp note\n",
      "cutoff-scheme           = Verlet    ; Historically the default setting had been Group which is no longer supported\n",
      "\n",
      "\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "sed 's/tc-grps                 = Protein Non-Protein/tc-grps = system/' input/simulation-settings/md-amber.mdp |\n",
    "sed 's/tau_t                   = 0.1     0.1/tau_t = 0.1/' |\n",
    "sed 's/ref_t                   = 300     300/ref_t = 300/' |\n",
    "sed '/nsteps/d' |\n",
    "sed  '1ifree-energy             = yes'    |\n",
    "sed  '2iinit-lambda             = 0'      |\n",
    "sed  '3idelta-lambda            = 4e-5 ; we want delta-lambda * nsteps = 1'  | \n",
    "sed  '4insteps                  = 25000  ; this has to match detla-lambda' |\n",
    "sed  '5isc-alpha                = 0.3'    | \n",
    "sed  '6isc-sigma                = 0.25'   |\n",
    "sed  '7isc-power                = 1'      |\n",
    "sed  '8isc-coul                 = yes' |\n",
    "sed  '9instcalcenergy           = 1' |\n",
    "sed '10instdhdl                 = 1' |\n",
    "sed '4ifep-lambdas            = 0.0 0.0 0.0 0.0 0.4 0.5 0.6 0.7 0.8 1.0' |\n",
    "sed '5icoul-lambdas           = 0.0 0.2 0.5 1.0 1.0 1.0 1.0 1.0 1.0 1.0' |\n",
    "sed '6irestraint-lambdas      = 0.0 0.0 0.1 0.2 0.3 0.5 0.7 1.0 1.0 1.0' |\n",
    "sed '11icontinuation            = yes\\n' > ti-eth-prop.mdp\n",
    "cat ti-eth-prop.mdp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we follow the usual scheme of setting up simulations by first generating an input file with grompp, then carrying out the actual simulation. We give the command for running a larger series of simulations commented below - for the sake of time in this tutorial we will per default only run one transition."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 230,
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "turning H bonds into constraints...\n",
      "turning H bonds into constraints...\n",
      "turning H bonds into constraints...\n",
      "turning H bonds into constraints...\n",
      "Analysing residue names:\n",
      "There are:     1      Other residues\n",
      "There are:   965      Water residues\n",
      "There are:     6        Ion residues\n",
      "Analysing residues not classified as Protein/DNA/RNA/Water and splitting into groups...\n",
      "Analysing residues not classified as Protein/DNA/RNA/Water and splitting into groups...\n",
      "Determining Verlet buffer for a tolerance of 0.005 kJ/mol/ps at 300 K\n",
      "Calculated rlist for 1x1 atom pair-list as 1.035 nm, buffer size 0.035 nm\n",
      "Set rlist, assuming 4x4 atom pair-list, to 1.000 nm, buffer size 0.000 nm\n",
      "Note that mdrun will redetermine rlist based on the actual pair-list setup\n",
      "Calculating fourier grid dimensions for X Y Z\n",
      "Using a fourier grid of 32x32x32, spacing 0.110 0.110 0.110\n",
      "This run will generate roughly 4 Mb of data\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "                      :-) GROMACS - gmx grompp, 2018.1 (-:\n",
      "\n",
      "                            GROMACS is written by:\n",
      "     Emile Apol      Rossen Apostolov      Paul Bauer     Herman J.C. Berendsen\n",
      "    Par Bjelkmar    Aldert van Buuren   Rudi van Drunen     Anton Feenstra  \n",
      "  Gerrit Groenhof    Aleksei Iupinov   Christoph Junghans   Anca Hamuraru   \n",
      " Vincent Hindriksen Dimitrios Karkoulis    Peter Kasson        Jiri Kraus    \n",
      "  Carsten Kutzner      Per Larsson      Justin A. Lemkul    Viveca Lindahl  \n",
      "  Magnus Lundborg   Pieter Meulenhoff    Erik Marklund      Teemu Murtola   \n",
      "    Szilard Pall       Sander Pronk      Roland Schulz     Alexey Shvetsov  \n",
      "   Michael Shirts     Alfons Sijbers     Peter Tieleman    Teemu Virolainen \n",
      " Christian Wennberg    Maarten Wolf   \n",
      "                           and the project leaders:\n",
      "        Mark Abraham, Berk Hess, Erik Lindahl, and David van der Spoel\n",
      "\n",
      "Copyright (c) 1991-2000, University of Groningen, The Netherlands.\n",
      "Copyright (c) 2001-2017, The GROMACS development team at\n",
      "Uppsala University, Stockholm University and\n",
      "the Royal Institute of Technology, Sweden.\n",
      "check out http://www.gromacs.org for more information.\n",
      "\n",
      "GROMACS is free software; you can redistribute it and/or modify it\n",
      "under the terms of the GNU Lesser General Public License\n",
      "as published by the Free Software Foundation; either version 2.1\n",
      "of the License, or (at your option) any later version.\n",
      "\n",
      "GROMACS:      gmx grompp, version 2018.1\n",
      "Executable:   /usr/bin/gmx\n",
      "Data prefix:  /usr\n",
      "Working dir:  /home/cblau/Projects/tutorials/fast-growth-ti/data\n",
      "Command line:\n",
      "  gmx grompp -o ti-eth-prop0.tpr -f ti-eth-prop.mdp -t ethanol-split0.trr -p topol.top -c ethanol-split0.gro\n",
      "\n",
      "Ignoring obsolete mdp entry 'title'\n",
      "\n",
      "NOTE 1 [file ti-eth-prop.mdp]:\n",
      "  With PME there is a minor soft core effect present at the cut-off,\n",
      "  proportional to (LJsigma/rcoulomb)^6. This could have a minor effect on\n",
      "  energy conservation, but usually other effects dominate. With a common\n",
      "  sigma value of 0.34 nm the fraction of the particle-particle potential at\n",
      "  the cut-off at lambda=0.5 is around 3.9e-05, while ewald-rtol is 1.0e-05.\n",
      "\n",
      "Setting the LD random seed to 1696080888\n",
      "Generated 2628 of the 2628 non-bonded parameter combinations\n",
      "Generating 1-4 interactions: fudge = 0.5\n",
      "Generated 2628 of the 2628 1-4 parameter combinations\n",
      "Excluding 3 bonded neighbours molecule type 'ethanol'\n",
      "Excluding 2 bonded neighbours molecule type 'SOL'\n",
      "Excluding 1 bonded neighbours molecule type 'NA'\n",
      "Excluding 1 bonded neighbours molecule type 'CL'\n",
      "Removing all charge groups because cutoff-scheme=Verlet\n",
      "Number of degrees of freedom in T-Coupling group System is 5832.00\n",
      "Reading Coordinates, Velocities and Box size from old trajectory\n",
      "Will read whole trajectory\n",
      "trr version: GMX_trn_file (single precision)\n",
      "\r",
      "Reading frame       0 time    0.000   \r",
      "Last frame          0 time    0.000   \n",
      "Using frame at t = 0 ps\n",
      "Starting time for run is 0 ps\n",
      "Estimate for the relative computational load of the PME mesh part: 0.35\n",
      "\n",
      "There was 1 note\n",
      "\n",
      "Back Off! I just backed up ti-eth-prop0.tpr to ./#ti-eth-prop0.tpr.2#\n",
      "\n",
      "GROMACS reminds you: \"Engage\" (J.L. Picard)\n",
      "\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "gmx grompp -o ti-eth-prop0.tpr -f ti-eth-prop.mdp -t ethanol-split0.trr -p topol.top -c ethanol-split0.gro"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 232,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "                      :-) GROMACS - gmx mdrun, 2018.1 (-:\n",
      "\n",
      "                            GROMACS is written by:\n",
      "     Emile Apol      Rossen Apostolov      Paul Bauer     Herman J.C. Berendsen\n",
      "    Par Bjelkmar    Aldert van Buuren   Rudi van Drunen     Anton Feenstra  \n",
      "  Gerrit Groenhof    Aleksei Iupinov   Christoph Junghans   Anca Hamuraru   \n",
      " Vincent Hindriksen Dimitrios Karkoulis    Peter Kasson        Jiri Kraus    \n",
      "  Carsten Kutzner      Per Larsson      Justin A. Lemkul    Viveca Lindahl  \n",
      "  Magnus Lundborg   Pieter Meulenhoff    Erik Marklund      Teemu Murtola   \n",
      "    Szilard Pall       Sander Pronk      Roland Schulz     Alexey Shvetsov  \n",
      "   Michael Shirts     Alfons Sijbers     Peter Tieleman    Teemu Virolainen \n",
      " Christian Wennberg    Maarten Wolf   \n",
      "                           and the project leaders:\n",
      "        Mark Abraham, Berk Hess, Erik Lindahl, and David van der Spoel\n",
      "\n",
      "Copyright (c) 1991-2000, University of Groningen, The Netherlands.\n",
      "Copyright (c) 2001-2017, The GROMACS development team at\n",
      "Uppsala University, Stockholm University and\n",
      "the Royal Institute of Technology, Sweden.\n",
      "check out http://www.gromacs.org for more information.\n",
      "\n",
      "GROMACS is free software; you can redistribute it and/or modify it\n",
      "under the terms of the GNU Lesser General Public License\n",
      "as published by the Free Software Foundation; either version 2.1\n",
      "of the License, or (at your option) any later version.\n",
      "\n",
      "GROMACS:      gmx mdrun, version 2018.1\n",
      "Executable:   /usr/bin/gmx\n",
      "Data prefix:  /usr\n",
      "Working dir:  /home/cblau/Projects/tutorials/fast-growth-ti/data\n",
      "Command line:\n",
      "  gmx mdrun -s ti-eth-prop0.tpr\n",
      "\n",
      "\n",
      "Back Off! I just backed up md.log to ./#md.log.11#\n",
      "Compiled SIMD: SSE2, but for this host/run AVX2_256 might be better (see log).\n",
      "The current CPU can measure timings more accurately than the code in\n",
      "gmx mdrun was configured to use. This might affect your simulation\n",
      "speed as accurate timings are needed for load-balancing.\n",
      "Please consider rebuilding gmx mdrun with the GMX_USE_RDTSCP=ON CMake option.\n",
      "Reading file ti-eth-prop0.tpr, VERSION 2018.1 (single precision)\n",
      "Changing nstlist from 10 to 50, rlist from 1 to 1.108\n",
      "\n",
      "\n",
      "Using 1 MPI thread\n",
      "Using 4 OpenMP threads \n",
      "\n",
      "\n",
      "Back Off! I just backed up ener.edr to ./#ener.edr.8#\n",
      "\n",
      "Back Off! I just backed up dhdl.xvg to ./#dhdl.xvg.2#\n",
      "starting mdrun 'ethanol in water'\n",
      "25000 steps,     50.0 ps.\n",
      "\n",
      "Writing final coordinates.\n",
      "\n",
      "Back Off! I just backed up confout.gro to ./#confout.gro.3#\n",
      "\n",
      "               Core t (s)   Wall t (s)        (%)\n",
      "       Time:     1250.505      312.626      400.0\n",
      "                 (ns/day)    (hour/ns)\n",
      "Performance:       13.819        1.737\n",
      "\n",
      "GROMACS reminds you: \"There's a limit to how many times you can read how great you are and what an inspiration you are, but I'm not there yet.\" (Randy Pausch)\n",
      "\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "gmx mdrun -s ti-eth-prop0.tpr"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 238,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "50796  3536\r\n"
     ]
    }
   ],
   "source": [
    "!sum dhdl.xvg"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Evaluate work values using fast-growth"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
