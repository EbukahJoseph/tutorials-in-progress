{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Overview - Calculating the relative free energy of binding for two different ligands\n",
    "\n",
    "```\n",
    "Author        : Christian Blau \n",
    "Goal          : Get an overview of methods to calculate the relative free energy of binding of two different ligands to the same protein   \n",
    "Time          : 30 minutes \n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Table of Contents\n",
    "\n",
    "  * Introduction\n",
    "    * [Reading material](#references)\n",
    "    * [The challenge - LUSH](#challenge)\n",
    "  * Theory\n",
    "    * [The thermodynamic cycle](#strategies)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Reading material and references<a class=\"anchor\" id=\"references\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### General methodology\n",
    "Gapsys, Vytautas, et al. \"Calculation of binding free energies.\" Molecular Modeling of Proteins. Humana Press, New \n",
    "York, NY, 2015. 173-209. [pdf](http://www3.mpibpc.mpg.de/groups/de_groot/pdf/Gapsys_MolMod_2015.pdf)\n",
    "\n",
    "\n",
    "#### Original theoretical derivation\n",
    "\n",
    "G. Crooks, \"Entropy production fluctuation theorem and the nonequilibrium work relation for free energy differences\", Physical Review E, 60, 2721 (1999) [pdf](https://arxiv.org/pdf/cond-mat/9901352)\n",
    "\n",
    "\n",
    "#### Reference values for the free energy of solvation\n",
    "\n",
    "Cabani, Sergio, et al. \"Group contributions to the thermodynamic properties of non-ionic organic solutes in dilute aqueous solution.\" Journal of Solution Chemistry 10.8 (1981): 563-595. [springer link](https://link.springer.com/article/10.1007/BF00646936)\n",
    "\n",
    "#### Absolute binding free energy calculation\n",
    "\n",
    "Aldeghi, Matteo, et al. \"Accurate calculation of the absolute free energy of binding for drug molecules.\" Chemical science 7.1 (2016): 207-218. [rcslink](https://pubs.rsc.org/--/content/articlehtml/2016/sc/c5sc02678d)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true
   },
   "source": [
    "# The challenge - does ethanol or n-propanol bind better to LUSH<a class=\"anchor\" id=\"challenge\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "The working horse for these tutorial is LUSH, an alcohol-binding protein from Drosophila melanogaster. It binds ethanol and n-propanol. In this tutorial, we will calculate which one of the two alcohols will bind more strongly. Read more about LUSH in the original [publication](https://www.nature.com/articles/nsb960) where the two X-ray structures below are published."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "![Lushwithethnolandpropanol](images/lush-propanol-ethanol.svg)\n",
    "*Protein LUSH (white cartoon) with and ethanol (pdb-id `1OOF`, salmon sticks, left) and propanol (pdb-id `1OOG`, light blue sticks) bound, respectively.*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "The aim of the tutorial is to answer the question:\n",
    "\n",
    "**In a dilute mixture with equal parts ethanol and propanol and some LUSH proteins, what is the expected ratio of LUSH proteins with ethanol bound to ones with propanol bound?**\n",
    "\n",
    "Now, we translate this question into equations and simulation setups."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The thermodynamic cycle to calculate relative binding free energies<a class=\"anchor\" id=\"strategies\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Thermodynamic Cycle](images/free-energy-of-binding.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To address the question which of two ligands binds better to a protein, we have a look at how hard it is for a ligand to move from out of the solution and bind to the protein, reflected by the relative free energy $\\Delta G$ for the process. The reference state is thus the ligand in solution as depicted below. \n",
    "\n",
    "![](images/ethanol-propanol.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ligand A (ethanol, red) binds better than ligand B (propanol, blue) if the free energy difference of moving the ligand from solution to the protein binding site is smaller for A, than for B, $\\Delta G_3 < \\Delta G_4$, or \n",
    "\n",
    "$$\\Delta G_3 - \\Delta G_4 = \\Delta\\Delta G_{\\mathrm{bind}} < 0 ?$$\n",
    "\n",
    "$\\Delta\\Delta G_{\\mathrm{bind}}$ is the one number we are after in this tutorial. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Calculating the free energy difference this way, though possible (done [here](https://pubs.rsc.org/en/content/articlelanding/2016/sc/c5sc02678d)), is very compute intensive and costly. The reason for this is the large change in the system. If you're interested in further considerations, have a look at the tutorial that describes the solvation free energy of ethanol. \n",
    "\n",
    "We can get away with a much smaller change in the system by using the thermodynamic cycle that is depicted below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Thermodynamic Cycle](images/thermodynamic-cycle.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When calculating a total free energy difference between two states in thermodynamic equilibrium it cannot matter what path we take in a thermodynamic cycle. Thus, the total free energy change in the system from A$\\cdot$aq to B$\\cdot$protein must be the same, whether we take the orange or the green path, \n",
    "$$\\Delta G_3 + \\Delta G_2 = \\Delta G_1 + \\Delta G_4 $$\n",
    "$$\\Delta G_3 - \\Delta G_4 = \\Delta G_1 - \\Delta G_2 = \\Delta\\Delta G_{\\mathrm{bind}}$$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The difference in relative binding free energy can thus be evaluated as the difference of \n",
    "\n",
    " * $\\Delta G_1$, the free energy change in the system from changing ethanol to propanol in solution\n",
    " * $\\Delta G_2$, the free energy change in the system from changing ethanol to propanol when binding to a protein "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To read $\\Delta G_1$ from experimentally determined solvation free energies, we use the thermodynamic cycle below ![tcycle vapour](images/thermodynamic-cycle-vapour.svg)\n",
    "\n",
    "|Compound   |  $\\Delta G^0_h$ at 25$^\\circ$C kJ$\\,$mol$^{\\mathrm{-1}}$| $\\Delta G_1$ |\n",
    "|-----------|---------|------|\n",
    "| Ethanol   |  -20.98 | 0.00 |\n",
    "| 1-Propanol|  -20.19 | 0.79 |\n",
    "| 1-Butanol |  -19.73 | 1.25 | \n",
    "\n",
    "Butler, J. A. V., C. N. Ramchandani, and D. W. Thomson. \"58. The solubility of non-electrolytes. Part I. The free energy of hydration of some aliphatic alcohols.\" Journal of the Chemical Society (Resumed) (1935): 280-285. [Article](https://pubs-rsc-org.ezp.sub.su.se/ja/content/articlepdf/1935/jr/jr9350000280)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In a second stage, we will repeat the process for $\\Delta G_2$ that involves the protein instead of a simple solution environment, but follows the very same basic principles."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Technical foundations: parametrisation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ligands like 1-propanol are usually not included in the original force-field parametrization process. Thus, you will have to determine these force-field parameters yourself. There are, however a number of tools available that will guide you during the process. For a brief overview on how to proceed with our system, have a look at the [parameterization tutorial](parametrization.ipynb)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Technical foundations: dummy atoms and dual topologies"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To calculate free energy differences between checmical species, we have to do some special preparations to our simulation input.\n",
    "\n",
    "In \"vanilla\" molecular dynamics simulations, atoms cannot be created or anihilated arbitrarily and the systems are described with a fixed topology. To remedy this, topologies can have A and B states, where some atoms can have no interactions in one state, though interact in another state. This \"trick\" allows for the change in chemistry during the curse \n",
    "of the simulation, while still simulating with a fixed number of atoms."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We prepared a special tutorial that explains thow to treat and place dummy atoms and [create hybrid topologies](hybrid-topology.ipynb)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Free energy calculation methods"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We explore the most common methods for calculating the relative free energy between two compounds. This will work in general for all thermodynamic systems that have a state A and B. In these tutorials we will use ethanol for state A (blue) and 1-propanol in state B (red) and associate $\\lambda = 0$ with state A and $\\lambda=1$ to state B."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![states](images/fepmethods-states.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We use the following symbols to denote the simulation setups and evaluation results used for the different free energy methods:\n",
    "![states](images/fepmethods-legend.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Simple FEP"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The oldest established method relies on sampling the system in state A, then comparing the energies of these configurations in state A and state B. See the simulation setup and values to evaluate below"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![states](images/fepmethods-simplefep.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\\Delta G_{AB} = -\\frac{1}{\\beta}\\log\\left\\langle\\exp(-\\beta (H_B(\\vec{x})-H_A(\\vec{x}))\\right\\rangle_A$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In words: for each configuration, calculate the exponentiated difference of the free energies. Then take the $\\log$ of the average of these values over the trajectory. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Pros**\n",
    " - technically the simplest form\n",
    " - need to run just one simulation\n",
    " - only needs energy calculation\n",
    " - no need to generate a starting structure for the B state\n",
    " \n",
    "**Cons**\n",
    " - very bad performance\n",
    " - no sampling in B state will mean that reasonable configurations will be missed"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Stratified FEP"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To aleviate the issue of bad overlap between sampled states A and B, it is possible to use that the total free energy change can be split up as a sum of changes going through intermediate states."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![stratification](images/fepmethods-stratification.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The simulation setup would look like this"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![states](images/fepmethods-stratifiedfep.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Pros**\n",
    " - technically still easy to set up\n",
    " - only needs energy calculation\n",
    " \n",
    "**Cons**\n",
    "  - still very bad performance\n",
    "  - need to generate a starting structure for the intermediate states"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Simple BAR"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The next oldest method makes use of configurations in the B state as well. The basic idea starts from a simple FEP that calculates the $\\Delta G$ from looking at the energy differences in ensemble A (blue line) and B (red line) at the same time. It finds the optimal free energy difference that reconciles these. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![states](images/fepmethods-simplebar.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\\Delta G_{AB} = \\frac1\\beta \\log \\cfrac{\\sum_{x\\in A} f\\left[H_A(\\vec{x})- H_B(\\vec{x})+C\\right] / n_A} {\\sum_{x\\in B} f\\left[H_B(\\vec{x})- H_A(\\vec{x})-C\\right] / n_B } +C $$\n",
    "\n",
    "with\n",
    "\n",
    "$$ C = \\Delta G_{AB} - \\frac1\\beta \\log \\frac{n_A}{n_B} ; f[x]= \\cfrac{1}{1+\\exp(\\beta x)} $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While this looks complicated on first sight, it is solved numerically by the recipe below. Note that $C$ directly reflects the free energy, only corrected for different numbers of samples in A and B."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The equation to solve for $C$ is\n",
    "\n",
    "$$ \\sum_{x\\in A} f[H_B(x) - H_A(x)-C] = \\sum_{x\\in B} f[H_A(x) - H_B(x)+C]$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In words: simulate the system in states A and B, then calculate energies for both. Keep track of the energy differences for all configurations from the A-state ensemble, calculated with A and B-state energy and all the configurations from the B-state ensemble calculated with the A- and B-state energy terms. Then use these energy differences to solve above equation numerically for $\\Delta G_{AB}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note** You don't have to solve these equations yourself, but can use tools like `gmx bar` aid you in solving these equations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Pros**\n",
    " - better performance than simple FEP\n",
    "\n",
    "**Cons**\n",
    " - Still largely dependent on ensemble overlap"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Stratified BAR"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similar to stratified FEP, we can apply the same concept to the BAR method. The simulation setup then looks like this"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![fepmethods](images/fepmethods-stratifiedbar.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, we replicate the BAR  setup as above, but perform a series of small-change free energy estimates. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Pros**\n",
    " - combines the benifits of BAR and stratification\n",
    "\n",
    "**Cons**\n",
    " - does not make the optimal used of the sampled configurations (see mBAR)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## multistate BAR"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Starting from stratified BAR, we can imagine skipping an in-between lambda state and employ a full matrix of free energy differences between $\\lambda=0,\\frac13, \\frac23, 1$ to find a best estimate to the overall free energy difference. Instead of jumping to conclusions from the free energy differences, we use the raw data of all energies in all simulations to find an estimate for the overall free energy difference. This method uses the same configurational sampling as BAR, but caculates all-vs-all energies."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![](images/fepmethods-mbar.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "mBAR solves for the different free energies at each $\\lambda$ in a self-consistent fashion. Upon solving these equations, the $G_{i}$ at a $\\lambda_i$ are determined up to a common additive constant. **Note** that only the free energy **differences** are meaningful. The set of equations to solve is\n",
    "\n",
    "$$G_{i} = -\\beta \\log \\sum_{j\\in\\mathrm{simulations}} \\sum_{n\\in\\mathrm{configurations}_j} \\cfrac{\\exp(- \\beta H_k(x^j_n))} {\\sum_{k\\in\\mathrm{simulations}} n_k \\exp(G_k - \\beta H_k(x^j_n)) } $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " mBAR also works when the energy matrix element $H_k(x^j_n)$, the energies of configurations of simulation $j$ with the energy function of state $k$ are not sampled. For this scenario, $n_k = 0$. In fact, in practice only a few neighbouring $\\lambda$ states will be considered."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, you typically do not have to solve the equation yourself, but can benefit from solutions provided by tools like pymbar that are part of the larger [alchemical analysis package](https://github.com/MobleyLab/alchemical-analysis) and it's successor library [alchemlyb](https://github.com/alchemistry/alchemlyb)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Change of attack: from H(x) to dH/d$\\lambda$(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All previous methods rely on evaluating, the energy of the system, the Hamiltonian, to estimate free energy differences, culminating in mBAR as the most robust and complex free energy estimator.\n",
    "\n",
    "The following methods change the angle of attack. They rely on the derivative of the Hamiltonian with respect to a $\\lambda$ parameter that descibes the change from one structure to another and estimate free energy differences more from a perspective that emphasises the energy input that is needed to change the system state as described by $\\lambda$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Thermodynamic Integration"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Thermodynamic integration slowly changes the system state A to state B, assuming that the system at each $\\lambda$ point is in equilibrium. We keep track of the change in energy of the system due to a change in state as described by dH/d$\\lambda$ for each configuration x. To indicate that we keep track of dH/d$\\lambda$ we will use here and further on a dashed line."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![TI](images/fepmethods-ti.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The free energy difference between two states is defined as\n",
    "$$\\Delta G = \\int_{\\lambda=0}^1 \\left\\langle\\cfrac{\\mathrm{d}H}{\\mathrm{d}\\lambda}(x)\\right\\rangle_\\lambda \\,\\mathrm{d}\\lambda $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In words: continuously encrease $\\lambda$, but so slowly that when binning $\\lambda$ values, the averaged $\\cfrac{\\mathrm{d}H}{\\mathrm{d}\\lambda}$ is a good approximation to the true ensemble average at a fixed $\\lambda$. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Cons**\n",
    " - computationally expensive\n",
    " - has a systematic error for non-infinite sampling times due to the steadily moving $\\lambda$ value"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Discrete TI"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Approximating the integral given above as a sum over $\\lambda$ values, yields \"discrete\" thermodynamic integration."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\\Delta G = \\sum_{\\lambda=0}^1 \\left\\langle\\cfrac{\\mathrm{d}H}{\\mathrm{d}\\lambda}\\right\\rangle_\\lambda \\,\\Delta\\lambda $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The simulation setup changes in that we run multiple simulations at fixed $\\lambda$ values, then average $\\cfrac{\\mathrm{d}H}{\\mathrm{d}\\lambda}$ over the course of a simulation. The dashed line indicates that we evaluate dH/d$\\lambda$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![dti](images/fepmethods-dti.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Pros**\n",
    " - better approximation than slow-growth TI\n",
    "\n",
    "**Cons**\n",
    " - need to prepare system at different starting points\n",
    " - need some overlap between states between $\\lambda$ (tight $\\lambda$ spacing) to reasonably approximate the integral that yields the free energy.\n",
    " - still bad performance compared to other methods"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Jarzynski equation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Instead of trying to get an equilibrium ensemble average, we can make use of the fact that the work values required to transform a system from different configurations from state A to state B and the free energy difference between these states are linked. The work itself is given directly by an integral over dH/d$\\lambda$:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$W=\\int_{\\lambda=0}^1 \\cfrac{\\mathrm{d}H}{\\mathrm{d}\\lambda}(x)\\,\\mathrm{d}\\lambda$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this setting, multiple simulations are carried out that record each the work it took them to go from state A to state B."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![jarzinsky](images/fepmethods-jarzynski.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The total free energy difference for a number of $N$ simulations is then\n",
    "$$\\Delta G = -\\frac1\\beta \\log \\left( \\frac1N \\sum_i \\exp(-\\beta W_i)\\right)$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While this sum converges for infinitely many simulations, note that there is a bias when calculating the free energy this way. \n",
    "\n",
    "The challenge in the Jarzynski estimator lies in the exponential in the sum - very few work values contribute a lot to the overall free energy estimate."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Crooks fluctuation theorem - fast-growth"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly as BAR symmetrises the free energy calculation setup from the basic FEP setup, we can symmetrise the setup of the Jarzynski estimator above. With the Crooks fluctuation theorem, we can make use of the complete distribution of work values to evaluate the free energy difference between two states.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![CFT](images/fepmethods-cft.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From the different work values for forward, $W^f$, and reverse transition, $W^r$, we can extract a probability distribution $P_f(W)$ and $P_r(W)$. The Crooks fluctuation theorem connects these probability distributions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\\log \\cfrac{P_f(W)}{P_r(W)} = \\beta(W-\\Delta G) $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While it is possible to read the free energy from these work probability distributions, we fare even better with an estimater that directly uses the evaluated work. It is the same BAR estimater that we encountered above in the BAR mechanism, however now using Work values instead of differences in Hamiltonians:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$ C = \\Delta G_{AB} - \\frac1\\beta \\log \\frac{n_f}{n_r} ; f[x]= \\cfrac{1}{1+\\exp(\\beta x)} $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Numerically solving for C will give the best estimate on the free energy difference"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$ \\sum_{i=1}^{n_f} f[W^f_i - C] = \\sum_{i=1}^{n_r} f[-W^r_j+C]$$"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
