title                   = Amber NPT 

; Parameters describing what to do, when to stop and what to save
integrator              = md        ; leap-frog integrator
dt                      = 0.002     ; 2 fs
nsteps                  = 50000     ; 2 * 50000 = 100 ps
nstenergy               = 1000      ; save energy and temperature every 2ps
nstxout-compressed      = 1000      ; save every 2ps

; Pressure coupling is on
pcoupl                  = Parrinello-Rahman     ; Pressure coupling on in NPT
pcoupltype              = isotropic             ; uniform scaling of box vectors
tau_p                   = 2.0                   ; time constant, in ps
ref_p                   = 1.0                   ; reference pressure, in bar
compressibility         = 4.5e-5                ; isothermal compressibility of water, bar^-1
refcoord_scaling        = com

; Keep system temperature fluctuating physically correct
tcoupl                  = V-rescale           ; modified Berendsen thermostat
tc-grps                 = Protein Non-Protein ; two coupling groups - more accurate
tau_t                   = 0.1     0.1         ; time constant, in ps
ref_t                   = 300     300         ; reference temperature, one for each group, in K

; Settings that make sure we run with parameters in harmony with the selected force-field
constraints             = h-bonds   ; bonds involving H are constrained
rcoulomb                = 1.0       ; short-range electrostatic cutoff (in nm)
rvdw                    = 1.0       ; short-range van der Waals cutoff (in nm)
vdw-modifier            = Potential-shift-Verlet ; Amber specific
DispCorr                = EnerPres  ; account for cut-off vdW scheme
coulombtype             = PME       ; Particle Mesh Ewald for long-range electrostatics
fourierspacing          = 0.125     ; grid spacing for FFT

; Setting to avoid grompp note
cutoff-scheme           = Verlet    ; Historically the default setting had been Group which is no longer supported


